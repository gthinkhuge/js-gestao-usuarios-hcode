class UserController {

    constructor(formId, tableId) {
        this.formEl = document.getElementById(formId);
        this.tableEl = document.getElementById(tableId);

        this.onSubmit();
    }

    onSubmit() {
        //arrow function (como não tem mais a palavra function o scopo não muda, portanto a palavra 'this' faz refêrencia ainda a classe UserController)
        this.formEl.addEventListener("submit", (event) => {
            //cancelar comportamento padrão do elemento
            event.preventDefault();

            //values retorna o conteúdo do User (um objeto com valores)
            let values = this.getValues();
            //The then() method returns a Promise. It takes up to two arguments (resolve, reject) callback functions for the success and failure cases of the Promise.
            //método then() o primeiro parâmetro é uma função que executará no caso de tudo der certo, a segunda função é caso dê erro
            //é preciso usar arrow function pois a palavra "function" muda o escopo da palavra "this"
            this.getPhoto().then((content)=>{
                values.photo = content;

                this.addLine(values);
            }, (e)=>{
                //comando que exibe mensagem como erro
                console.error(e);
            });

        });
    }

    getPhoto() {
        // a Promise é uma classe por isso precisa ser instânciada, por isso a palavra "new" antes de executar um Promise
        return new Promise((resolve, reject)=>{

            let fileReader = new FileReader();

            let elements = [...this.formEl.elements].filter(item => {

                if (item.name === 'photo') {
                    return item;
                }

            });

            let file = elements[0].files[0];

            fileReader.onload = ()=>{

                resolve(fileReader.result);
            };

            fileReader.onerror = (e) =>{

                reject(e);
            };

            fileReader.onerror = (e) =>{
                //evento da Promise caso dê erro e o "e" para saber o tipo de erro
                reject(e);
            };

            //se não tiver um arquivo de foto não enviar
            if (file) {
                fileReader.readAsDataURL(file);
            } else {
                //preciso colocar o resolve se não todos os outros campos também não serão enviados
                //em vez do file (que não existe pois o usuario não passou) colocamos um caminho para uma imagem de placeholder
                resolve('dist/img/boxed-bg.jpg');
            }
        });

    }

    getValues(){

        let user = {};

        //transformo o this.formEl.elements em um array para poder aplicar o forEach
        //os 3 pontos é chamado spread e serve para que eu não precise escrever quantos elementos tem no array
        [...this.formEl.elements].forEach(function(field, index){

            if (field.name == "gender") {

                if (field.checked){
                    //adiciona dentro do objeto user declarado fora desta função o field.name dinamicamente
                    user[field.name] = field.value;
                }

            } else {
                //basicamente em vez de field[i] como seria em um "for i < field.length" passo o valor do field e o seu atributo que é o name.
                user[field.name] = field.value;
            }

        });

        //Objeto é a cópia da classe criado com a palavra new
        //Um objeto é uma variável que instância uma classe
        return new User(
            user.name,
            user.gender,
            user.birth,
            user.country,
            user.email,
            user.password,
            user.photo,
            user.admin
        );

    }

    //passamos o parâmetro dataUser e acessaremos os atributos dele dentro do innerHTML
    addLine(dataUser){

        this.tableEl.innerHTML = `
            <tr>
               <td><img src="${dataUser.photo}" alt="User Image" class="img-circle img-sm"></td>
               <!-- é preciso colocar as variaveis dentro de '$'{} para o JS entender que são variáveis e não texto simples -->
               <td>${dataUser.name}</td>
               <td>${dataUser.email}</td>
               <td>${dataUser.admin}</td>
               <td>${dataUser.birth}</td>
               <td>
                 <button type="button" class="btn btn-primary btn-xs btn-flat">Editar</button>
                 <button type="button" class="btn btn-danger btn-xs btn-flat">Excluir</button>
               </td>
             </tr>
        `;
    }

}
